#!/bin/bash

mkdir -p static/data/countries
headers=$(sed -n 1p static/data/outlook.csv)

declare -a countries=(
AUS AUT BEL CAN 
CZE DNK FIN FRA 
DEU GRC HUN ISL 
IRL ITA JPN KOR 
LUX MEX NLD NZL 
NOR POL PRT SVK 
ESP SWE CHE TUR 
GBR USA OTO NMEC 
BRA CHL CHN COL 
EST IND IDN ISR 
LVA RUS SVN ZAF 
WLD DAE OOP OIL 
IRL CRI NOR LTU 
SVK EA16 ARG CRI
)

for i in "${countries[@]}"
do
    echo $i
    awk "/\"${i}\"/ {print}" static/data/outlook.csv | echo $headers"$(cat -)" > static/data/countries/${i}.csv
    # awk "/\"${i}\"/ {print}" data/consumer_indicator.csv | echo $indicator_header"$(cat -)" | node_modules/csvtojson/bin/csvtojson > dist/out/${i}_consumer_indicator.json
    # awk "/\"${i}\"/ {print}" data/business_indicator.csv | echo $indicator_header"$(cat -)" | node_modules/csvtojson/bin/csvtojson > dist/out/${i}_business_indicator.json
    # awk "/\"${i}\"/ {print}" data/composite_indicator.csv | echo $indicator_header"$(cat -)" | node_modules/csvtojson/bin/csvtojson > dist/out/${i}_composite_indicator.json
done
