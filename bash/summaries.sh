#!/bin/bash
headers=$(sed -n 1p static/data/outlook.csv)

awk '/2017|2018/ {print}' static/data/outlook.csv | awk '/GDPV_ANNPCT|CBGDPR|\"UNR\"|GGFLMQ|CTGSVD/ {print}' \
| echo $headers"$(cat -)" > static/data/summaries.csv

awk '// {print}' static/data/outlook.csv | awk '/GDPV_ANNPCT|CBGDPR|\"UNR\"|GGFLMQ/ {print}' \
| echo $headers"$(cat -)" > static/data/summaries.full.csv