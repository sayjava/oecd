FROM node:latest

# create the app context
RUN mkdir -p /app

# add global nodemon
# RUN npm i -g forever

WORKDIR /app

COPY package.json /app/

RUN yarn

COPY . .

RUN chmod 700 bash/countries.sh
RUN chmod 700 bash/summaries.sh

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

RUN yarn data
RUN yarn build

ARG PORT=80
ENV PORT $PORT
EXPOSE $PORT

# ENTRYPOINT [ "yarn", "start" ]