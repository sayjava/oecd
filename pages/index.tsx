import styles from "./index.scss";

import SummaryTable from "../components/tables/summary-table/table";
import Footer from "../components/page/footer";
import { outlook } from "../lib/economic";

export default () => {
    const year = new Date().getFullYear();
    return (
        <div className={styles.summary}>
            <h1 className={styles.pageTitle}>World Economy {year}</h1>
            <div className={styles.summaryTable}>
                <h2 className={styles.summaryTitle}>Economic Zones</h2>
                <SummaryTable year={year} countries={outlook.aggregates} />
            </div>
            <div className={styles.summaryTable}>
                <h2 className={styles.summaryTitle}>OECD Countries</h2>
                <SummaryTable year={year} countries={outlook.countries} />
            </div>
            <Footer />
        </div>
    );
};
