import Document, { Head, Main, NextScript } from "next/document";

export default class MyDocument extends Document {
    public render() {
        return (
            <html>
                <Head>
                    <title>World Economy </title>
                    <meta charset="utf-8" />
                    {/* <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" /> */}
                    <link rel="stylesheet" href="/_next/static/style.css" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                </Head>
                <body>
                    <div className="app">
                        <Main />
                        <NextScript />
                    </div>
                </body>
            </html>
        );
    }
}
