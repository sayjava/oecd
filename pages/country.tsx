import React from "react";
import { dataToCountry, outlook } from "../lib/economic";
import { primaries } from "../lib/colors";
import { IOECDCountry, getEconomicsYear, IEconomicYear } from "../lib/country";

import SummaryLine from "../components/summary-line";
import Footer from "../components/page/footer";
import Summary from "../components/econ-summary/summary";

import styles from "./country.scss";

const PapaParse = require("papaparse");

const fetchOnServer = (country: string) => {
    const csv = require("fs")
        .readFileSync(`static/data/countries/${country.toUpperCase()}.csv`)
        .toString();
    return Promise.resolve(PapaParse.parse(csv, { header: true, dynamicTyping: true }).data);
};

const fetchOnClient = async country => {
    const href = `${location.origin}/static/data/countries/${country.toUpperCase()}.csv`;
    const csv = await fetch(href).then(res => res.text());
    return PapaParse.parse(csv, { header: true, dynamicTyping: true }).data;
};

export default class Country extends React.Component<{ country: IOECDCountry; year: number }> {
    public static async getInitialProps({ req, query: { country, year } }) {
        const json = !req ? await fetchOnClient(country) : await fetchOnServer(country);
        const [oecd] = dataToCountry(json);
        return { country: oecd, year };
    }

    public static getAggregate(code) {
        return outlook.aggregates.filter(c => c.code === code)[0];
    }

    public eu: IOECDCountry;
    public economics: IEconomicYear;
    public prevEconomics: IEconomicYear;
    public viewKey: string;

    constructor(args) {
        super(args);
        const { year, country } = this.props;
        this.economics = getEconomicsYear(year, country);
        this.prevEconomics = getEconomicsYear(year - 1, country);
        this.eu = Country.getAggregate("EA16");
        this.viewKey = country.name.toLocaleLowerCase();
    }

    public render() {
        const { country, year } = this.props;
        return (
            <div className="root">
                <h3 className={styles.title}>
                    {country.name} {year}
                </h3>
                <div className={styles.country}>
                    <div className={styles.summary}>
                        <div className={styles.itemSummary}>
                            <Summary
                                prev={this.prevEconomics.gdpGrowth}
                                current={this.economics.gdpGrowth}
                                title="GDP  Growth"
                            />
                        </div>
                        <div className={styles.itemSummary}>
                            <Summary
                                prev={this.prevEconomics.unemployment}
                                current={this.economics.unemployment}
                                title="Unemployment"
                            />
                        </div>
                        <div className={styles.itemSummary}>
                            <Summary
                                prev={{ Value: this.prevEconomics.worldTradeShare.Value * 100 }}
                                current={{ Value: this.economics.worldTradeShare.Value * 100 }}
                                title="% World Trade"
                            />
                        </div>
                        <div className={styles.itemSummary}>
                            <Summary
                                prev={this.prevEconomics.publicDebt}
                                current={this.economics.publicDebt}
                                title="Debt to GDP"
                            />
                        </div>
                    </div>

                    <div className={styles.fullLineGraph}>
                        <div className={styles.content}>
                            <div className={styles.header}>
                                <h3>GDP Growth</h3>
                                <span
                                    className={styles.countryLegend}
                                    style={{ margin: 0, borderBottom: `2px solid ${primaries[0]}` }}
                                >
                                    {country.name}
                                </span>
                                <span
                                    className={styles.countryLegend}
                                    style={{ borderBottom: `2px solid ${primaries[1]}` }}
                                >
                                    EU16
                                </span>
                            </div>
                            <SummaryLine data={[country.economics.gdpGrowth, this.eu.economics.gdpGrowth]} />
                        </div>
                    </div>

                    <div className={styles.fullLineGraph}>
                        <div className={styles.content}>
                            <div className={styles.header}>
                                <h3>Unemployment</h3>
                                <span
                                    className={styles.countryLegend}
                                    style={{ margin: 0, borderBottom: `2px solid ${primaries[0]}` }}
                                >
                                    {country.name}
                                </span>
                                <span
                                    className={styles.countryLegend}
                                    style={{ borderBottom: `2px solid ${primaries[1]}` }}
                                >
                                    EU16
                                </span>
                            </div>
                            <SummaryLine data={[country.economics.unemployment, this.eu.economics.unemployment]} />
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}
