import React from "react";
import { outlook } from "../lib/economic";
import { ResponsiveTreeMap } from "@nivo/treemap";

export default class WorldTreee extends React.Component {
    public render() {
        const getValue = trade => {
            return ((trade[0] || {}).Value || 0).toFixed(2);
        };
        const data = {
            id: "trade",
            name: "World Trade",
            color: "hsl(280, 70%, 50%)",
            children: outlook.countries.map(country => ({
                id: country.code,
                code: country.code,
                name: country.name,
                size: getValue(country.economics.pctGlobalTrade),
                y: getValue(country.economics.pctGlobalTrade),
                x: country.name
            }))
        };

        const getLabel = d => `${d.code} ${d.size}`;

        data.children = data.children.sort((a, b) => b.size - a.size);

        return (
            <div className="page">
                <h3 className="title">Trade Contributions</h3>
                <div className="treemap">
                    <ResponsiveTreeMap
                        id="trade"
                        title="slice"
                        root={data}
                        identity="name"
                        value="size"
                        innerPadding={2}
                        outerPadding={2}
                        colorBy="name"
                        label={getLabel}
                        labelSkipSize={30}
                        labelTextColor="inherit:darker(2.5)"
                        colors={["#A4B1C0", "#BAC4CE", "#B1BAC6", "#9FACB9", "#5B6C7D"]}
                        borderColor="inherit:darker(2.3)"
                    />
                </div>
            </div>
        );
    }
}
