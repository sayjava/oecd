import Aggregates from "./aggregates";

export interface IRowData {
    VALUE?: number;
    Value?: number;
    VARIABLE?: string;
    Variable?: string;
    Country?: string;
    LOCATION?: string;
    Time?: number;
    TIME?: string;
    Unit?: any;
}

export const extractNameAndCode = (row: IRowData = {}): any => {
    return {
        name: row.Country ? row.Country : "Unknown",
        code: row.LOCATION ? row.LOCATION : "Unknown",
        isAggregate: row.LOCATION ? Aggregates.includes(row.LOCATION) : true
    };
};
