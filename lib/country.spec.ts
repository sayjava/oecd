import { dataToCountry, getEconomicsYear } from "./country";

const fixtures = [
    { Country: "Canada", value: 2, LOCATION: "CAN", VARIABLE: "GDPV_ANNPCT", TIME: "2009" },
    { Country: "Canada", value: 4, LOCATION: "CAN", VARIABLE: "GDPV_ANNPCT", TIME: "2008" },
    { Country: "Canada", value: 4, LOCATION: "CAN", VARIABLE: "GDPV_ANNPCT", TIME: "2007" },
    { Country: "Canada", value: 10, LOCATION: "CAN", VARIABLE: "UNR", TIME: "2009" },
    { Country: "Italy", value: 12, LOCATION: "ITA", VARIABLE: "GDPV_ANNPCT" },
    { Country: "Germany", value: 1, LOCATION: "DEU", VARIABLE: "GDPV_ANNPCT" },
    { Country: "DEU", value: 1, LOCATION: "" }
];

test("data to country", () => {
    const countries = dataToCountry(fixtures);
    expect(countries).toMatchSnapshot();
});

test("get the economics for a year", () => {
    const country = dataToCountry(fixtures)[0];
    expect(getEconomicsYear(2009, country)).toMatchSnapshot();
});
