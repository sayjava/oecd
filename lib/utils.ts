export const groupBy = (data: any[], key: string): Map<string, any> => {
    const groupings: Map<string, any> = data.reduce((acc: Map<string, any>, row: any) => {
        if (!acc.has(row[key])) {
            acc.set(row[key], new Array());
        }
        acc.get(row[key]).push(row);
        return acc;
    }, new Map());

    return groupings;
};
