import { extractNameAndCode } from "./rowdata";

test("that an unknown data piece is returned", () => {
    const row = {
        Country: "Canada",
        LOCATION: "CAN"
    };
    const data = extractNameAndCode(row);
    expect(data).toMatchSnapshot();
});

test("that its an aggregate", () => {
    const row = {
        Country: "EU16",
        LOCATION: "EA16"
    };
    const data = extractNameAndCode(row);
    expect(data).toMatchSnapshot();
});
