import { groupBy } from "./utils";

test("r", () => {
    const array = [
        { name: "CAN", value: 2 },
        { name: "CAN", value: 4 },
        { name: "BOL", value: 12 },
        { name: "DEU", value: 1 }
    ];
    const groups = groupBy(array, "name");
    expect(groups).toMatchSnapshot();
});
