import { IEconomics } from "./economics";
import { groupBy } from "./utils";
import { extractNameAndCode, IRowData } from "./rowdata";

export interface IOECDCountry {
    name: string;
    code: string;
    isAggregate: boolean;
    economics: IEconomics;
}

export interface IEconomicYear {
    unemployment?: IRowData;
    interestRate?: IRowData;
    shortInterestRate?: IRowData;
    fiscalBalance?: IRowData;
    gdpGrowth?: IRowData;
    govtFinancialGdp?: IRowData;
    govtInterestGdp?: IRowData;
    netExportGoodsService?: IRowData;
    netExportGdp?: IRowData;
    exportGoodsServicesGrowth?: IRowData;
    totalDosmeticExpenditure?: IRowData;
    coreInflation?: IRowData;
    govtLendingGdp?: IRowData;
    wage?: IRowData;
    householdSavingsRatio?: IRowData;
    cpi?: IRowData;
    purchasingPower?: IRowData;
    labourForce?: IRowData;
    labourEffeciency?: IRowData;
    worldTradeShare?: IRowData;
    gdpCapita?: IRowData;
    publicDebt?: IRowData;
    pctGlobalTrade?: IRowData;
}

const getProperty = (property: string, row: Map<string, any[]>) => (row.get(property) || []).reverse();

const rowToCountry = (row: Map<string, any[]>): IOECDCountry => {
    const economics: IEconomics = {
        unemployment: getProperty("UNR", row),
        interestRate: getProperty("IRL", row),
        shortInterestRate: getProperty("IRS", row),
        fiscalBalance: getProperty("CBGDPR", row),
        gdpGrowth: getProperty("GDPV_ANNPCT", row),
        govtFinancialGdp: getProperty("GNFLQ", row),
        govtInterestGdp: getProperty("GNINTQ", row),
        netExportGoodsService: getProperty("FBGS", row),
        netExportGdp: getProperty("CQ_FBGSV", row),
        exportGoodsServicesGrowth: getProperty("XGSV_ANNPCT", row),
        totalDosmeticExpenditure: getProperty("TDDV_ANNPCT", row),
        coreInflation: getProperty("PCORE_YTYPCT", row),
        govtLendingGdp: getProperty("NLGQ", row),
        wage: getProperty("WAGE", row),
        householdSavingsRatio: getProperty("TRPTSH", row),
        cpi: getProperty("CPI", row),
        purchasingPower: getProperty("PPP", row),
        labourForce: getProperty("LFPRS1574", row),
        labourEffeciency: getProperty("EFFLABS", row),
        worldTradeShare: getProperty("SHTGSVD", row),
        gdpCapita: getProperty("GDPVD_CAP", row),
        publicDebt: getProperty("GGFLMQ", row),
        pctGlobalTrade: getProperty("CTGSVD", row)
    } as IEconomics;

    return {
        economics,
        ...extractNameAndCode(economics.gdpGrowth[0])
    };
};

export const dataToCountry = (rows): IOECDCountry[] => {
    const countries: IOECDCountry[] = [];
    const groupByCountry = groupBy(rows, "LOCATION");

    // delete empties
    groupByCountry.delete("");

    groupByCountry.forEach(values => {
        const countryVariables = groupBy(values, "VARIABLE");
        countries.push(rowToCountry(countryVariables));
    });

    return countries;
};

export const getEconomicsYear = (year: number, country: IOECDCountry): IEconomicYear => {
    const filterRowForYear = (row: IRowData[]) => {
        const rowYears = row.filter(data => Number(data.Time) === Number(year));
        return rowYears[0] || { Value: 0 };
    };

    return Object.keys(country.economics).reduce((acc, key) => {
        acc[key] = filterRowForYear(country.economics[key]);
        return acc;
    }, {}) as IEconomicYear;
};
