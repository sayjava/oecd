export const textToClass = {
    GGFLMQ: {
        text: 'Public debt % of GDP',
        class: 'debt',
        description: 'The public debt of a GDP',
        isPositive: (value) => (value > 70 ? 'negative' : ''),
    },
    GDPV_ANNPCT: {
        text: 'GDP Growth',
        class: 'gdp',
        description: 'How the economy was growing',
        isPositive: (value) => (value > 0 ? '' : 'negative'),
    },
    UNR: {
        text: 'Unemployment rate',
        class: 'employment',
        description: 'How many people at work',
        isPositive: (value) => (value > 10 ? 'negative' : ''),
    },
    CBGDPR: {
        text: 'Fiscal Balance',
        class: 'balance',
        description: 'How much a country is selling',
        isPositive: (value) => (value < 0 ? 'negative' : ''),
    },
    PCORE_YTYPCT: {
        text: 'Inflation',
        class: 'inflation',
        description: 'The inflation figure',
        isPositive: () => '',
    },
    BCI: {
        text: 'Business Indicator',
        class: 'business_indicator',
        description: 'Business Indicator',
        isPositive: () => '',
    },
    CCI: {
        text: 'Consumer Indicator',
        class: 'consumer_indicator',
        description: 'Consumer Indicator',
        isPositive: () => '',
    },
    CLI: {
        text: 'Composite Indicator',
        class: 'composite_indicator',
        description: 'Composite Indicator',
        isPositive: () => '',
    },
};
