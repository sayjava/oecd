import { IRowData } from "./rowdata";

export interface IEconomics {
    unemployment?: IRowData[];
    interestRate?: IRowData[];
    fiscalBalance?: IRowData[];
    gdpGrowth?: IRowData[];
    publicDebt?: IRowData[];
    govtFinancialGdp?: IRowData[];
    govtInterestGdp?: IRowData[];
    shortInterestRate?: IRowData[];
    netExportGoodsService?: IRowData[];
    netExportGdp?: IRowData[];
    exportGoodsServicesGrowth?: IRowData[];
    totalDosmeticExpenditure?: IRowData[];
    coreInflation?: IRowData[];
    wage?: IRowData[];
    householdSavingsRatio?: IRowData[];
    govtLendingGdp?: IRowData[];
    cpi?: IRowData[];
    purchasingPower?: IRowData[];
    labourForce?: IRowData[];
    labourEffeciency?: IRowData[];
    worldTradeShare?: IRowData[];
    pctGlobalTrade?: IRowData[];
    gdpCapita?: IRowData[];
}
