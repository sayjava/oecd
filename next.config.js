const withTypescript = require("@zeit/next-typescript");
const withSass = require("@zeit/next-sass");

const merge = require("webpack-merge");

const webPackConfig = {
    webpack(config, options) {
        return merge(config, {
            module: {
                rules: [
                    {
                        test: /\.csv$/,
                        loader: "csv-loader",
                        options: {
                            dynamicTyping: true,
                            header: true,
                            skipEmptyLines: true
                        }
                    }
                ]
            }
        });
    },
    distDir: "build"
};

const sassConfig = {
    cssLoaderOptions: {
        importLoaders: 1,
        localIdentName: "[name]__[local]"
    },
    cssModules: true
};

module.exports = withSass(
    withTypescript({
        ...sassConfig,
        webpack(config, options) {
            return merge(config, {
                module: {
                    rules: [
                        {
                            test: /\.csv$/,
                            loader: "csv-loader",
                            options: {
                                dynamicTyping: true,
                                header: true,
                                skipEmptyLines: true
                            }
                        }
                    ]
                }
            });
        },
        distDir: "build"
    })
);
