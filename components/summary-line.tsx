import React from "react";
import { ResponsiveLine } from "@nivo/line";
import { IRowData } from "../lib/rowdata";
import { primaries } from "../lib/colors";

const dataToChart = (rows: IRowData[]) => {
    const [first] = rows;
    return {
        id: `${first.Variable} - ${first.Country}`,
        data: rows
            .concat()
            .reverse()
            .map(row => ({
                id: row.VARIABLE,
                x: String(row.Time),
                y: Number(row.Value.toFixed(2))
            }))
    };
};

export default class SummaryLine extends React.Component<{ data: IRowData[][] }> {
    public render() {
        const series = this.props.data.map(dataToChart);
        const margin = {
            top: 30,
            right: 20,
            bottom: 100,
            left: 20
        };
        const getLabel = d => `${d.y}%`;
        return (
            <ResponsiveLine
                data={series}
                colors={primaries}
                curve="basis"
                enableGridX={false}
                stacked={false}
                animate={true}
                minY="auto"
                dotSize={10}
                dotColor="inherit:darker(0.3)"
                dotBorderWidth={2}
                dotBorderColor="#ffffff"
                enableDotLabel={true}
                enableArea={true}
                areaOpacity={0.05}
                margin={margin}
                axisLeft={null}
                dotLabel={getLabel}
            />
        );
    }
}
