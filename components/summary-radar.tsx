import { ResponsiveRadar } from "@nivo/radar";
import { IEconomicYear } from "lib/country";

const margin = {
    top: 0,
    right: 80,
    bottom: 60,
    left: 80
};

const getValue = d => `%${d.value}`;

const countryToRadar = (country: string, economics: IEconomicYear) => {
    const { publicDebt, unemployment, gdpGrowth, fiscalBalance } = economics;
    return [
        {
            measure: "Employment",
            [country]: Number((100 - unemployment.Value).toFixed(2)),
            model: 120
        },
        {
            measure: "Debt",
            [country]: Number(publicDebt.Value.toFixed(2)),
            model: 120
        },
        {
            measure: "Fiscal Balance",
            [country]: Number((100 - fiscalBalance.Value).toFixed(2)),
            model: 120
        },
        {
            measure: "Growth",
            [country]: Number(gdpGrowth.Value.toFixed(2)),
            model: 120
        }
    ];
};

export default ({ economics, keys }) => (
    <ResponsiveRadar
        indexBy="measure"
        margin={margin}
        colors="#9EA5AD"
        curve="cardinalClosed"
        gridShape="circular"
        data={countryToRadar(keys[0], economics)}
        keys={keys}
        colorBy="key"
        gridLevels={4}
        gridLabelOffset={25}
        enableDotLabel={true}
        dotLabel={getValue}
        enableDots={true}
    />
);
