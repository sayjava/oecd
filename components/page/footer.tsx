import styles from "./footer.scss";

export default () => (
    <div className={styles.container}>
        <h3 className={styles.title}>OECD Economics</h3>
        <span className={styles.text}>
            All economic data is sourced from © 2018
            <a href="https://data.oecd.org">OECD Data</a>
        </span>
        <ul className={styles.links}>
            <li>
                <a href="https://data.oecd.org">Twitter</a>
            </li>
            <li>
                <a href="https://data.oecd.org">Facebook</a>
            </li>
            <li>
                <a href="https://data.oecd.org">Email</a>
            </li>
            <li>
                <a href="https://data.oecd.org">Linked</a>
            </li>
        </ul>
    </div>
);
