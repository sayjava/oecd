import { IRowData } from "../../lib/rowdata";

import styles from "./summary.scss";

const calChange = (prev: IRowData, current: IRowData) => (current.Value - prev.Value) / current.Value * 100;

const getNegative = val => (val < 0 ? styles.negative : "");

export default ({ prev, current, title }) => (
    <div className={styles.container}>
        <h4 className={styles.title}>{title}</h4>
        <span className={styles.current}>{current.Value.toFixed(2)}%</span>
        <span className={styles.change}>
            <span className={`${styles.prev} ${getNegative(calChange(prev, current))}`}>
                {calChange(prev, current).toFixed(2)}%
            </span>
            <span> YOY</span>
        </span>
    </div>
);
