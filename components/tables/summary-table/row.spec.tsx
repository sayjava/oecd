import Adapter from "enzyme-adapter-react-16";
import Enzyme, { shallow } from "enzyme";
import React from "react";
import Row from "./row";
import { IEconomicYear } from "lib/country";

Enzyme.configure({ adapter: new Adapter() });

const economics: IEconomicYear = {
    gdpGrowth: { Value: 5, LOCATION: "CAN", Country: "Canada" },
    unemployment: { Value: 10 },
    fiscalBalance: { Value: 3 }
};

test("Summary row renderered", () => {
    expect(shallow(<Row economics={economics} linkable />)).toMatchSnapshot();
});

test("Summary row renderered as unlinkable", () => {
    expect(shallow(<Row economics={economics} linkable={false} />)).toMatchSnapshot();
});
