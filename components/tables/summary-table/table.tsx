import React from "react";
import { IOECDCountry, getEconomicsYear } from "../../../lib/country";
import CountryRow from "./row";

import styles from "./table.scss";

interface ITableProps {
    countries: IOECDCountry[];
    year: number;
}

interface ITableState {
    countries: IOECDCountry[];
    sort: {
        property: string;
        desc: boolean;
    };
    year: number;
}

export default class SummaryTable extends React.Component<ITableProps, ITableState> {
    constructor(args) {
        super(args);
        const { countries, year } = this.props;
        const sort = { property: "gdpGrowth", desc: true, year };

        this.state = {
            countries: this.sortCountries(countries, sort),
            sort,
            year
        };
    }

    public sortCountries(countries: IOECDCountry[], { property, desc, year }) {
        return countries.sort((c1, c2) => {
            const econYear1 = getEconomicsYear(year, c1);
            const econYear2 = getEconomicsYear(year, c2);

            const p1 = econYear1[property];
            const p2 = econYear2[property];

            return desc ? p2.Value - p1.Value : p1.Value - p2.Value;
        });
    }

    public doSort(sort) {
        // sort and store state
        this.setState(oldState => {
            return {
                ...oldState,
                countries: this.sortCountries(oldState.countries, sort),
                sort
            };
        });
    }

    public createHeader(property: string, title: string, sort) {
        // set the sorting direction
        const desc = property !== sort.property || !sort.desc;

        // register the sort functions
        const sortFunc = () => this.doSort({ property, desc, year: sort.year });

        return (
            <th onClick={sortFunc} className={`value ${property}`}>
                {title} %
            </th>
        );
    }

    public mapToRow(country) {
        const { year } = this.state;
        const econ = getEconomicsYear(year, country);
        return (
            <tr key={country.code}>
                <CountryRow year={year} linkable={country.isAggregate} economics={econ} />
            </tr>
        );
    }

    public render() {
        const { countries, sort } = this.state;
        return (
            <div className={styles.container}>
                <table className={styles.table}>
                    <thead>
                        <tr>
                            <th className={styles.tableName}>Country</th>
                            {this.createHeader("gdpGrowth", "GDP Growth", sort)}
                            {this.createHeader("unemployment", "Unemployment", sort)}
                            {this.createHeader("fiscalBalance", "Fiscal Balance", sort)}
                        </tr>
                    </thead>
                    <tbody>{countries.map(this.mapToRow.bind(this))}</tbody>
                </table>
            </div>
        );
    }
}
