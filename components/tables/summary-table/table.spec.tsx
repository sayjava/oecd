import Adapter from "enzyme-adapter-react-16";
import React from "react";
import Enzyme, { shallow } from "enzyme";

Enzyme.configure({ adapter: new Adapter() });

import Table from "./table";

const countries = [
    {
        name: "Canada",
        code: "CAN",
        isAggregates: false,
        economics: {
            gdpGrowth: [
                {
                    Value: 5,
                    Country: "CAN",
                    Time: 2008
                }
            ],
            unemployment: [
                {
                    Value: 10,
                    Country: "CAN",
                    Time: 2008
                }
            ],
            fiscalBalance: [
                {
                    Value: 10,
                    Country: "CAN",
                    Time: 2008
                }
            ]
        }
    },
    {
        name: "Finland",
        code: "FIN",
        isAggregates: false,
        economics: {
            gdpGrowth: [
                {
                    Value: 5,
                    Country: "FIN",
                    Time: 2008
                }
            ],
            unemployment: [
                {
                    Value: 2,
                    Country: "FIN",
                    Time: 2008
                }
            ],
            fiscalBalance: [
                {
                    Value: -1,
                    Country: "FIN",
                    Time: 2008
                }
            ]
        }
    }
];

test("that summary table is rendered", () => {
    const component = shallow(<Table year={2008} countries={countries} />);
    expect(component).toMatchSnapshot();
});

test("should sort the table", () => {
    const component = shallow(<Table year={2008} countries={countries} />);
    component.find(".unemployment").simulate("click");
    expect(component).toMatchSnapshot();
});
