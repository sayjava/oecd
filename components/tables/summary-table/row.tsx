import React from "react";
import Link from "next/link";
import { IEconomicYear } from "lib/country";

import styles from "./table.scss";

interface IRowProps {
    economics: IEconomicYear;
    linkable: boolean;
    year: number;
}

const CountryRow = ({ economics, linkable, year }: IRowProps) => {
    const { gdpGrowth, unemployment, fiscalBalance } = economics;

    const link = linkable ? (
        <span>{gdpGrowth.Country}</span>
    ) : (
        <Link href={{ pathname: "/country", query: { year, country: gdpGrowth.LOCATION } }} prefetch>
            <a className={styles.tableLink}>{gdpGrowth.Country}</a>
        </Link>
    );

    return (
        <React.Fragment>
            <td className={styles.tableName}>{link}</td>
            <td className={styles.tableValue}>{gdpGrowth.Value.toFixed(2)}</td>
            <td className={styles.tableValue}>{unemployment.Value.toFixed(2)}</td>
            <td className={`${styles.tableValue} ${fiscalBalance.Value < 0 ? styles.tableValueNegative : ""}`}>
                {fiscalBalance.Value.toFixed(2)}
            </td>
        </React.Fragment>
    );
};

export default CountryRow;
