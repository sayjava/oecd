import * as next from "next";

const express = require("express");
const dev = process.env.NODE_ENV !== "production";

export default (distDir = "build") => {
    const server = express();

    server.use(express.static("static"));

    const conf = dev ? { dev } : { dev: false, conf: { distDir } };
    const app = next(conf);

    const handle = app.getRequestHandler();
    app.prepare().then(() => {
        server.get("/", (req, res) => {
            app.render(req, res, "/", req.query);
        });

        server.get("/indicators", (req, res) => {
            app.render(req, res, "/indicators", req.query);
        });

        server.get("/trade", (req, res) => {
            app.render(req, res, "/worldtrade", req.query);
        });

        server.get("/about", (req, res) => {
            app.render(req, res, "/about", req.query);
        });

        server.get("*", (req, res) => {
            return handle(req, res);
        });
    });

    return server;
};
